# binenv

## Usage

`binenv install <app> [version] [--interactive]`
`binenv uninstall <app> [version] [--yes]`
`binenv global <app> <version> [--interactive]`
`binenv local <app> <version> [--interactive]`
`binenv update` => updates definitions
`binenv versions [app]`